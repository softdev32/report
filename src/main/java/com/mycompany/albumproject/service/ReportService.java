/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.albumproject.service;

import com.mycompany.albumproject.dao.SaleDao;
import com.mycompany.albumproject.model.ReportSale;
import java.util.List;

/**
 *
 * @author Windows10
 */
public class ReportService {
    public List<ReportSale> getReportSaleByDay(){
        SaleDao dao =new SaleDao();
        return dao.getDayReport();
    }

   public List<ReportSale> getReportSaleByMonth(){
        SaleDao dao =new SaleDao();
        return dao.getMonthReport();
    }
   
   public List<ReportSale> getReportSaleByYear(int year){
        SaleDao dao =new SaleDao();
        return dao.getYearReport(year);
    }
}
